use std::collections::HashSet;

use sokol_gp_sys::*;

use crate::{Map, Player, RayCaster};

pub struct World {
    player: Player,
    map: Map,
}

impl World {
    pub fn new<S: Into<String>>(map: S) -> Self {
        let map = Map::load(&format!("resources/{}.map", map.into())).unwrap();

        Self {
            player: Player::new(&map),
            map,
        }
    }

    pub fn player(&self) -> &Player {
        &self.player
    }

    pub fn player_mut(&mut self) -> &mut Player {
        &mut self.player
    }

    pub fn map(&self) -> &Map {
        &self.map
    }

    pub fn map_mut(&mut self) -> &mut Map {
        &mut self.map
    }

    pub fn draw(&self) {
        use crate::constants::*;

        for (i, ray) in RayCaster::new(&self.map, &self.player, None, None, false).enumerate() {
            let distance_in_players_direction = ray.len * ray.player_delta_angle.cos();
            let col_height = (MAIN_WINDOW_NO_SCALE_HEIGHT as f32
                / (distance_in_players_direction * 2.0))
                * PROJECTION_SCALE_FACTOR;
            let shading = (1.0 / (distance_in_players_direction * 0.3)).clamp(0.0, 1.0);
            let shading = if ray.hit_vertical_wall {
                shading
            } else {
                shading - 0.1
            };
            unsafe {
                sgp_set_color(
                    (2.0_f32 / 255.0) * shading,
                    (135.0_f32 / 255.0) * shading,
                    (195.0_f32 / 255.0) * shading,
                    1.0,
                );
                sgp_draw_filled_rect(
                    (i * PROJECTED_COLUMN_WIDTH) as f32 * PROJECTION_SCALE_FACTOR,
                    (MAIN_WINDOW_NO_SCALE_HEIGHT / 2) as f32 * PROJECTION_SCALE_FACTOR
                        - (col_height / 2.0),
                    PROJECTED_COLUMN_WIDTH as f32 * PROJECTION_SCALE_FACTOR,
                    col_height,
                );
            }
        }
    }

    pub fn draw_top_down(&self) {
        let mut traversed_tiles = HashSet::new();
        let mut cast_rays = Vec::new();
        for _ in RayCaster::new(
            &self.map,
            &self.player,
            Some(&mut traversed_tiles),
            Some(&mut cast_rays),
            true,
        ) {}

        self.map.draw_top_down();

        let player_tile = (
            (self.player.x() / self.map.px_per_tile() as f32).trunc() as i32,
            (self.player.y() / self.map.px_per_tile() as f32).trunc() as i32,
        );

        self.map
            .color_tile_top_down(player_tile.0, player_tile.1, 0.3, 0.2, 0.8);
        for tile in traversed_tiles.iter() {
            self.map.color_tile_top_down(tile.0, tile.1, 0.3, 0.2, 0.8);
        }

        for ray in cast_rays.iter() {
            let tile_intersection_point = ray.tile_intersection_point.unwrap_or_default();
            unsafe {
                sgp_set_color(1.0, 0.0, 0.0, 1.0);
                sgp_draw_line(
                    self.player.x(),
                    self.player.y(),
                    tile_intersection_point.0,
                    tile_intersection_point.1,
                );

                sgp_set_color(0.0, 1.0, 0.3, 1.0);
                sgp_draw_filled_rect(
                    tile_intersection_point.0 - 1.0,
                    tile_intersection_point.1 - 1.0,
                    2.0,
                    2.0,
                );
            }
        }

        self.player.draw_top_down();
    }
}
