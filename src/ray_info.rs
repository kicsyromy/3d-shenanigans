#[derive(Default, Copy, Clone)]
pub struct RayInfo {
    // The coordinates within a tile oriented towards the ray's look direction
    start_offset_in_tile: (f32, f32),
    // The value to increment tiles, calculated based on ray direction and angle
    x_y_inc_sign: (i8, i8),
    // The angle used to calculate the length of the ray up to a particular tile
    normalized_angle: f32,
}

impl RayInfo {
    pub fn new(x_y: (f32, f32), angle: f32, px_per_tile: u8) -> Self {
        let mut this = Self::default();
        this.update(x_y, angle, px_per_tile);

        this
    }

    pub fn update(&mut self, x_y: (f32, f32), angle: f32, px_per_tile: u8) {
        use crate::constants::*;

        let x_tile = x_y.0 / px_per_tile as f32;
        let y_tile = x_y.1 / px_per_tile as f32;

        // Quadrant 0
        if angle <= PI_DIV_2 && angle > 0.0 {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if (angle - PI_DIV_2).abs() <= f32::EPSILON {
                angle - f32::EPSILON
            } else {
                angle
            };

            self.start_offset_in_tile = (1.0 - x_tile.fract(), 1.0 - y_tile.fract());
            self.x_y_inc_sign = (1, 1);
            self.normalized_angle = angle;
        }
        // Quadrant 1
        else if angle <= PI && angle > PI_DIV_2 {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if (angle - PI).abs() <= f32::EPSILON {
                angle - f32::EPSILON
            } else {
                angle
            };

            self.start_offset_in_tile = (-1.0 * x_tile.fract(), 1.0 - y_tile.fract());
            self.x_y_inc_sign = (-1, 1);
            self.normalized_angle = PI_DIV_2 - (angle - PI_DIV_2);
        }
        // Quadrant 2
        else if angle <= THREE_PI_DIV_2 && angle > PI {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if (angle - THREE_PI_DIV_2).abs() <= f32::EPSILON {
                angle - f32::EPSILON
            } else {
                angle
            };

            self.start_offset_in_tile = (-1.0 * x_tile.fract(), -1.0 * y_tile.fract());
            self.x_y_inc_sign = (-1, -1);
            self.normalized_angle = angle - PI;
        }
        // Quadrant 3
        else {
            // If the player angle is exactly on the edge of the quadrant move it slightly inside
            // This helps work around some wierd calculation errors down the line
            let angle = if (angle - TWO_PI).abs() <= f32::EPSILON {
                angle - f32::EPSILON
            } else {
                angle
            };

            self.start_offset_in_tile = (1.0 - x_tile.fract(), -1.0 * y_tile.fract());
            self.x_y_inc_sign = (1, -1);
            self.normalized_angle = TWO_PI - angle;
        }
    }

    pub fn start_offset_x_in_tile(&self) -> f32 {
        self.start_offset_in_tile.0
    }

    pub fn start_offset_y_in_tile(&self) -> f32 {
        self.start_offset_in_tile.1
    }

    pub fn x_inc_sign(&self) -> f32 {
        self.x_y_inc_sign.0 as f32
    }

    pub fn y_inc_sign(&self) -> f32 {
        self.x_y_inc_sign.1 as f32
    }

    pub fn normalized_angle(&self) -> f32 {
        self.normalized_angle
    }
}
