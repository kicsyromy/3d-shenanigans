use nugget::events::*;

use crate::{FloatExtentions, PI_DIV_2};

pub struct Player {
    x: f32,
    y: f32,
    angle: f32,

    px_per_tile: u8,
    // The minimum distances to any obstacle before collision
    px_radius_x: f32,
    px_radius_y: f32,
}

impl Player {
    pub fn new(map: &crate::Map) -> Self {
        let player_properties = map.player_properties().unwrap();

        Player {
            x: player_properties.px_x_pos as _,
            y: player_properties.px_y_pos as _,
            angle: player_properties.angle,

            px_per_tile: map.px_per_tile(),
            px_radius_x: player_properties.px_width as f32 / 2.0,
            px_radius_y: player_properties.px_depth as f32 / 2.0,
        }
    }

    pub fn draw_top_down(&self) {
        use sokol_gp_sys::*;

        unsafe {
            sgp_set_color(0.0, 1.0, 0.0, 1.0);

            sgp_push_transform();
            {
                sgp_rotate_at(self.angle + PI_DIV_2, self.x, self.y);
                sgp_draw_filled_triangle(
                    self.x,
                    self.y - self.px_radius_y / 2.0,
                    self.x - self.px_radius_x / 2.0,
                    self.y + self.px_radius_y / 2.0,
                    self.x + self.px_radius_x / 2.0,
                    self.y + self.px_radius_y / 2.0,
                );
            }
            sgp_pop_transform();
        }
    }

    pub fn update_position(&mut self, event: &impl Event, magnitude: f32, map: &crate::Map) {
        use crate::constants::*;

        let is_collision = |x: f32, y: f32| {
            let (tile_x, tile_y) = (
                (x / map.px_per_tile() as f32).trunc() as i32,
                (y / map.px_per_tile() as f32).trunc() as i32,
            );
            map.tile_contains(tile_x, tile_y) != crate::LayoutObject::Empty
        };

        if event.is_key_pressed(KeyCode::Left) {
            self.angle -= self.px_per_tile as f32 / 16.0 * magnitude;
            if self.angle < 0.0 {
                self.angle += TWO_PI;
            }
        }

        if event.is_key_pressed(KeyCode::Right) {
            self.angle += self.px_per_tile as f32 / 16.0 * magnitude;
            if self.angle > TWO_PI {
                self.angle -= TWO_PI;
            }
        }

        if event.is_key_pressed(KeyCode::W) || event.is_key_pressed(KeyCode::Up) {
            let angle_cos = self.angle.cos();
            let angle_sin = self.angle.sin();

            let new_x = self.x + self.px_per_tile as f32 * 2.0 * angle_cos * magnitude;
            if !is_collision(new_x + self.px_radius_x * angle_cos, self.y) {
                self.x = new_x;
            }

            let new_y = self.y + self.px_per_tile as f32 * 2.0 * angle_sin * magnitude;
            if !is_collision(self.x, new_y + self.px_radius_y * angle_sin) {
                self.y = new_y;
            }
        }

        if event.is_key_pressed(KeyCode::S) || event.is_key_pressed(KeyCode::Down) {
            let angle_cos = self.angle.cos();
            let angle_sin = self.angle.sin();

            let new_x = self.x - self.px_per_tile as f32 * 2.0 * angle_cos * magnitude;
            if !is_collision(new_x - self.px_radius_x * angle_cos, self.y) {
                self.x = new_x;
            }

            let new_y = self.y - self.px_per_tile as f32 * 2.0 * angle_sin * magnitude;
            if !is_collision(self.x, new_y - self.px_radius_y * angle_sin) {
                self.y = new_y;
            }
        }

        if event.is_key_pressed(KeyCode::A) {
            let angle_cos = self.angle.cos();
            let angle_sin = self.angle.sin();

            let new_x = self.x + (self.px_per_tile as f32 / 8.0) * 10.0 * angle_sin * magnitude;
            if !is_collision(new_x + self.px_radius_x * angle_sin, self.y) {
                self.x = new_x;
            }

            let new_y = self.y - (self.px_per_tile as f32 / 8.0) * 10.0 * angle_cos * magnitude;
            if !is_collision(self.x, new_y - self.px_radius_y * angle_cos) {
                self.y = new_y;
            }
        }

        if event.is_key_pressed(KeyCode::D) {
            let angle_cos = self.angle.cos();
            let angle_sin = self.angle.sin();

            let new_x = self.x - (self.px_per_tile as f32 / 8.0) * 10.0 * angle_sin * magnitude;
            if !is_collision(new_x - self.px_radius_x * angle_sin, self.y) {
                self.x = new_x;
            }

            let new_y = self.y + (self.px_per_tile as f32 / 8.0) * 10.0 * angle_cos * magnitude;
            if !is_collision(self.x, new_y + self.px_radius_y * angle_cos) {
                self.y = new_y;
            }
        }
    }

    pub fn turn(&mut self, delta: f32) {
        use crate::constants::TWO_PI;

        self.angle += delta;

        if self.angle < 0.0 {
            self.angle += TWO_PI;
        }
        if self.angle > TWO_PI {
            self.angle -= TWO_PI;
        }
    }

    pub fn x(&self) -> f32 {
        self.x
    }

    pub fn y(&self) -> f32 {
        self.y
    }

    pub fn angle(&self) -> f32 {
        self.angle
    }
}
