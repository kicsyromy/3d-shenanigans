use std::collections::HashSet;

use crate::{constants::*, FloatExtentions, Map, Player, RayInfo};

// A ray that has been cast
#[derive(Default, Copy, Clone)]
pub struct CastRay {
    pub len: f32,
    pub player_delta_angle: f32,
    pub hit_vertical_wall: bool,
    pub tile_intersection_point: Option<(f32, f32)>,
}

pub struct RayCaster<'a> {
    map: &'a Map,
    player: &'a Player,

    index: usize,

    start_tile: (i32, i32),
    start_angle: f32,

    hit_test_fn: fn(i32, i32, f32, f32, &Map) -> Option<(f32, bool)>,

    // Data and structures used for debugging
    traversed_tiles: Option<&'a mut HashSet<(i32, i32)>>,
    cast_rays: Option<&'a mut Vec<CastRay>>,
    add_traversed_tile_fn: fn(&mut Option<&mut HashSet<(i32, i32)>>, i32, i32),
    add_cast_ray_fn: fn(&mut Option<&mut Vec<CastRay>>, ray: CastRay),
    find_tile_intersection_point_fn: fn(f32, u8, &RayInfo, &Player) -> Option<(f32, f32)>,
}

impl<'a> RayCaster<'a> {
    pub fn new(
        map: &'a Map,
        player: &'a Player,
        traversed_tiles: Option<&'a mut HashSet<(i32, i32)>>,
        cast_rays: Option<&'a mut Vec<CastRay>>,
        find_tile_intersection_point: bool,
    ) -> Self {
        let add_traversed_tile_fn = if traversed_tiles.is_some() {
            |hash_set: &mut Option<&mut HashSet<(i32, i32)>>, x: i32, y: i32| {
                let hash_set = hash_set.as_mut().unwrap();
                hash_set.insert((x, y));
            }
        } else {
            |_: &mut Option<&mut HashSet<(i32, i32)>>, _: i32, _: i32| {}
        };

        let add_cast_ray_fn = if cast_rays.is_some() {
            |rays: &mut Option<&mut Vec<CastRay>>, ray: CastRay| {
                let rays = rays.as_mut().unwrap();
                rays.push(ray);
            }
        } else {
            |_: &mut Option<&mut Vec<CastRay>>, _: CastRay| {}
        };

        let find_tile_intersection_point_fn = if find_tile_intersection_point {
            |ray_length: f32, px_per_tile: u8, ray_info: &RayInfo, player: &Player| {
                let angle_opposite_distance =
                    (ray_length * px_per_tile as f32) * ray_info.normalized_angle().sin();
                let angle_adjacent_distance =
                    (ray_length * px_per_tile as f32) * ray_info.normalized_angle().cos();

                Some((
                    player.x() + (ray_info.x_inc_sign() * angle_adjacent_distance),
                    player.y() + (ray_info.y_inc_sign() * angle_opposite_distance),
                ))
            }
        } else {
            |_: f32, _: u8, _: &RayInfo, _: &Player| None
        };

        let start_tile = (
            (player.x() / map.px_per_tile() as f32).trunc() as i32,
            (player.y() / map.px_per_tile() as f32).trunc() as i32,
        );

        Self {
            map,
            player,
            index: 0,

            start_tile,
            start_angle: player.angle() - ((PLAYER_FOV_DEG / 2) as f32).to_radians(),

            // Returns the ray length in grid units and if the ray hit a vertical wall
            hit_test_fn: |tile_x: i32,
                          tile_y: i32,
                          traversed_x: f32,
                          traversed_y: f32,
                          map: &Map| {
                let tile_content = map.tile_contains(tile_x, tile_y);
                if tile_content == crate::LayoutObject::Wall
                    || tile_content == crate::LayoutObject::Invalid
                {
                    Some(if traversed_x <= traversed_y {
                        (traversed_x, true)
                    } else {
                        (traversed_y, false)
                    })
                } else {
                    None
                }
            },

            cast_rays,
            traversed_tiles,
            add_traversed_tile_fn,
            add_cast_ray_fn,
            find_tile_intersection_point_fn,
        }
    }
}

impl Iterator for RayCaster<'_> {
    type Item = CastRay;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index >= PLAYER_FOV_DEG * RAYS_PER_DEGREE as usize {
            return None;
        }

        let ray_info = RayInfo::new(
            (self.player.x(), self.player.y()),
            self.start_angle.normalize_angle(),
            self.map.px_per_tile(),
        );

        // https://www.math.net/sohcahtoa
        let ray_angle_sin = ray_info.normalized_angle().sin();
        // The projected length of 1 tile unit, in the y axis, on the ray
        let ray_angle_inverse_sin = 1.0 / ray_angle_sin;
        let ray_angle_cos = ray_info.normalized_angle().cos();
        // The projected length of 1 tile unit, in the x axis, on the ray
        let ray_angle_inverse_cos = 1.0 / ray_angle_cos;

        let (mut x, mut y) = (
            ray_info.start_offset_x_in_tile(),
            ray_info.start_offset_y_in_tile(),
        );
        let mut traversed_x = x.abs() as f32 / ray_angle_cos;
        let mut traversed_y = y.abs() as f32 / ray_angle_sin;

        let cast_ray;
        loop {
            let (traversed_x_prev, traversed_y_prev) = (traversed_x, traversed_y);

            if traversed_x <= traversed_y {
                x += ray_info.x_inc_sign();
                // The ray moves 1 unit in the x axis, add the projected value to the length of the ray
                traversed_x += ray_angle_inverse_cos;
            } else {
                y += ray_info.y_inc_sign();
                // The ray moves 1 unit in the y axis, add the projected value to the length of the ray
                traversed_y += ray_angle_inverse_sin
            }

            let tile_x = self.start_tile.0 + x.trunc() as i32;
            let tile_y = self.start_tile.1 + y.trunc() as i32;
            {
                if let Some((traversed_distance, hit_vertical_wall)) = (self.hit_test_fn)(
                    tile_x,
                    tile_y,
                    traversed_x_prev,
                    traversed_y_prev,
                    &self.map,
                ) {
                    cast_ray = CastRay {
                        len: traversed_distance,
                        hit_vertical_wall,
                        player_delta_angle: self.player.angle() - self.start_angle,
                        tile_intersection_point: (self.find_tile_intersection_point_fn)(
                            traversed_distance,
                            self.map.px_per_tile(),
                            &ray_info,
                            &self.player,
                        ),
                    };
                    (self.add_cast_ray_fn)(&mut self.cast_rays, cast_ray.clone());

                    break;
                };

                (self.add_traversed_tile_fn)(&mut self.traversed_tiles, tile_x, tile_y);
            }
        }

        self.index += 1;
        self.start_angle += RAY_ANGLE_INCREMENT;

        Some(cast_ray)
    }
}
