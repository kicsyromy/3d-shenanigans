#![allow(dead_code)]

use std::{cell::RefCell, ops::Deref, rc::Rc};

use log::*;

use constants::*;
use map::*;
use nugget::events::*;
use nugget::*;
use player::*;
use ray_caster::*;
use ray_info::*;
use utility::*;
use world::*;

mod constants;
mod map;
mod player;
mod ray_caster;
mod ray_info;
mod utility;
mod world;

struct AppState {
    world: World,
}

impl AppState {
    fn new() -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Self {
            world: World::new("e1m1"),
        }))
    }

    fn take(this: &Rc<RefCell<Self>>) -> std::cell::Ref<Self> {
        RefCell::borrow(Rc::deref(this))
    }

    fn take_mut(this: &Rc<RefCell<Self>>) -> std::cell::RefMut<Self> {
        RefCell::borrow_mut(Rc::deref(this))
    }

    fn world(&self) -> &World {
        &self.world
    }

    fn world_mut(&mut self) -> &mut World {
        &mut self.world
    }
}

fn main() -> NuggetResult<()> {
    env_logger::builder()
        .filter_level(LevelFilter::Info)
        .try_init()
        .unwrap_or_default();

    let app = Application::new()?;
    let state = AppState::new();

    #[allow(unused_variables)]
    let main_window = WindowBuilder::new()
        .width(round_up(MAIN_WINDOW_NO_SCALE_WIDTH as f32 * PROJECTION_SCALE_FACTOR) as i32)
        .height(round_up(MAIN_WINDOW_NO_SCALE_HEIGHT as f32 * PROJECTION_SCALE_FACTOR) as i32)
        .title("3D Scene".into())
        .vsync(false)
        .on_key_pressed(enclose! { (state, app) move |window, key_press_event| {
            if key_press_event.key == KeyCode::Escape {
                if window.is_mouse_captured() {
                    window.release_mouse();
                } else {
                    app.write().unwrap().quit();
                }
            } else {
            }
        }})
        .on_mouse_button_pressed(move |window, mouse_btn_press_event| {
            if mouse_btn_press_event.button == MouseButton::Primary {
                window.capture_mouse();
            }
        })
        .on_mouse_moved(enclose! { (state) move |window, mouse_move_event| {
            let mut app_state = AppState::take_mut(&state);

            if window.is_mouse_captured() {
                let player = app_state.world_mut().player_mut();
                player.turn(mouse_move_event.rel_x as f32 * 0.005);
            }
        }})
        .on_render(enclose! { (state) move |window, render_event| {
            use sokol_gp_sys::*;

            let (width, height) = window.size();

            unsafe {
                sgp_begin(width, height);

                sgp_viewport(0, 0, width, height);
                sgp_project(0_f32, width as f32, 0_f32, height as f32);
            };

            unsafe {
                sgp_set_color(0.1_f32, 0.1_f32, 0.1_f32, 1.0_f32);
                sgp_clear();

                let mut app_state = AppState::take_mut(&state);

                let map = core::mem::take(app_state.world_mut().map_mut());
                app_state
                    .world_mut()
                    .player_mut()
                    .update_position(
                        render_event,
                        render_event.time_elapsed(),
                        &map
                    );
                let _ = core::mem::replace(app_state.world_mut().map_mut(), map);
                app_state.world_mut().draw();
            }

            unsafe {
                let pass_action: sg_pass_action = core::mem::zeroed();
                sg_begin_default_pass(&pass_action, width, height);
                sgp_flush();
                sgp_end();
                sg_end_pass();
                sg_commit();
            }
        }})
        .build()?;

    #[cfg(debug_assertions)]
    let px_per_tile = state.borrow().world().map().px_per_tile();

    #[cfg(debug_assertions)]
    #[allow(unused_variables)]
    let debug_window = WindowBuilder::new()
        .width(AppState::take(&state).world().map().tile_width() * px_per_tile as i32)
        .height(AppState::take(&state).world().map().tile_height() * px_per_tile as i32)
        .title("Top-Down View".into())
        .vsync(false)
        .on_render(enclose! { (state) move |window, render_event| {
            use sokol_gp_sys::*;

            let (width, height) = window.size();

            unsafe {
                sgp_begin(width, height);

                sgp_viewport(0, 0, width, height);
                sgp_project(0_f32, width as f32, 0_f32, height as f32);
            };

            unsafe {
                sgp_set_color(0.1_f32, 0.1_f32, 0.1_f32, 1.0_f32);
                sgp_clear();

                let app_state = AppState::take_mut(&state);
                app_state.world().draw_top_down();
            }

            unsafe {
                let pass_action: sg_pass_action = core::mem::zeroed();
                sg_begin_default_pass(&pass_action, width, height);
                sgp_flush();
                sgp_end();
                sg_end_pass();
                sg_commit();
            }
        }})
        .build()?;

    Application::exec(app)?;

    Ok(())
}
