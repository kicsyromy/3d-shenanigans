use std::io::{Read, Write};

#[rustfmt::skip]
macro_rules! as_byte_array {
    ($var: expr, $r#type: ty) => {
        unsafe {
            core::slice::from_raw_parts(
                $var as *const $r#type as *const u8,
                core::mem::size_of::<$r#type>(),
            )
        }
    };
}

#[rustfmt::skip]
macro_rules! as_byte_array_mut {
    ($var: expr, $r#type: ty) => {
        unsafe {
            core::slice::from_raw_parts_mut(
                $var as *mut $r#type as *mut u8,
                core::mem::size_of::<$r#type>(),
            )
        }
    };
}

#[rustfmt::skip]
macro_rules! vec_as_byte_array {
    ($var: expr, $r#type: ty) => {
        unsafe {
            core::slice::from_raw_parts(
                $var.as_ptr() as *const u8,
                $var.len() * core::mem::size_of::<$r#type>(),
            )
        }
    };
}

#[rustfmt::skip]
macro_rules! vec_as_byte_array_mut {
    ($var: expr, $len: expr, $r#type: ty) => {
        unsafe {
            core::slice::from_raw_parts_mut(
                $var.as_mut_ptr() as *mut u8,
                $len * core::mem::size_of::<$r#type>(),
            )
        }
    };
}

#[derive(Default, Eq, Ord, PartialEq, PartialOrd, Debug)]
#[repr(u8)]
pub enum Thing {
    Player = 0,
    Monster,
    Item,
    Decoration,
    #[default]
    Invalid = 255,
}

#[derive(Default, PartialEq, Copy, Clone, Debug)]
#[repr(C)]
pub struct ThingProperties {
    pub px_width: u8,
    pub px_depth: u8,
    pub px_height: u8,
    _unused: u8,

    pub px_x_pos: u16,
    pub px_y_pos: u16,

    pub angle: f32,

    pub resource_id: u16,

    pub hit_points: u16,
}

#[derive(Default, PartialEq, Copy, Clone, Debug)]
#[repr(u8)]
pub enum LayoutObject {
    Empty = 0,
    Wall,
    Door,
    #[default]
    Invalid = 255,
}

#[derive(Copy, Clone, PartialEq, Debug)]
#[repr(C)]
pub struct LayoutObjectProperties {
    pub resource_id: u16,
}

impl Default for LayoutObjectProperties {
    fn default() -> Self {
        Self {
            resource_id: u16::MAX,
        }
    }
}

#[derive(Default, PartialEq, Debug)]
#[repr(C)]
pub struct FileHeader {
    pub format: [u8; 4],
    pub version: u16,
    pub revision: u16,
}

#[derive(Default, PartialEq, Debug)]
#[repr(C)]
pub struct Header {
    pub tile_width: u16,
    pub tile_height: u16,
    pub px_per_tile: u8,
    pub thing_count: u16,
}

#[derive(Default, PartialEq, Debug)]
#[repr(C)]
pub struct Map {
    file_header: FileHeader,
    header: Header,

    things: Vec<Thing>,
    things_properties: Vec<ThingProperties>,

    layout: Vec<LayoutObject>,
    layout_properties: Vec<LayoutObjectProperties>,
}

impl Map {
    pub fn draw_top_down(&self) {
        use sokol_gp_sys::*;

        for y in 0..self.tile_height() {
            for x in 0..self.tile_width() {
                if self.layout[(y * self.tile_width() + x) as usize] == LayoutObject::Wall {
                    unsafe { sgp_set_color(1.0, 1.0, 1.0, 1.0) };
                } else {
                    unsafe { sgp_set_color(0.0, 0.0, 0.0, 1.0) };
                }
                unsafe {
                    sgp_draw_filled_rect(
                        (x * self.header.px_per_tile as i32) as f32,
                        (y * self.header.px_per_tile as i32) as f32,
                        self.header.px_per_tile as f32 - 1.0,
                        self.header.px_per_tile as f32 - 1.0,
                    );
                };
            }
        }
    }

    pub fn color_tile_top_down(&self, x: i32, y: i32, r: f32, g: f32, b: f32) -> bool {
        use sokol_gp_sys::*;

        if x < 0 || x >= self.tile_width() || y < 0 || y >= self.tile_height() {
            return false;
        }

        unsafe {
            sgp_set_color(r, g, b, 1.0);
            sgp_draw_filled_rect(
                (x * self.header.px_per_tile as i32) as f32,
                (y * self.header.px_per_tile as i32) as f32,
                self.header.px_per_tile as f32 - 1.0,
                self.header.px_per_tile as f32 - 1.0,
            );
        }

        true
    }

    #[inline]
    pub fn tile_width(&self) -> i32 {
        self.header.tile_width as i32
    }

    #[inline]
    pub fn tile_height(&self) -> i32 {
        self.header.tile_height as i32
    }

    #[inline]
    pub fn px_per_tile(&self) -> u8 {
        self.header.px_per_tile
    }

    pub fn player_properties(&self) -> Option<ThingProperties> {
        if let Ok(index) = self.things.binary_search(&Thing::Player) {
            Some(self.things_properties[index])
        } else {
            None
        }
    }

    pub fn tile_contains(&self, x: i32, y: i32) -> LayoutObject {
        if x < 0 || x >= self.tile_width() || y < 0 || y >= self.tile_width() {
            LayoutObject::Invalid
        } else {
            self.layout[(y * self.tile_width() + x) as usize]
        }
    }

    pub fn load(path: &str) -> Result<Map, Box<dyn std::error::Error>> {
        let mut file = std::fs::OpenOptions::new().read(true).open(path)?;

        let file_header = {
            let mut file_header = core::mem::MaybeUninit::<FileHeader>::uninit();
            file.read_exact(as_byte_array_mut!(file_header.as_mut_ptr(), FileHeader))?;
            unsafe { file_header.assume_init() }
        };
        assert_eq!(&file_header.format, &[b'K', b'M', b'A', b'P']);

        let header = {
            let mut header = core::mem::MaybeUninit::<Header>::uninit();
            file.read_exact(as_byte_array_mut!(header.as_mut_ptr(), Header))?;
            unsafe { header.assume_init() }
        };

        let things = {
            let mut things = Vec::<Thing>::new();
            things.reserve(header.thing_count as usize);
            file.read_exact(vec_as_byte_array_mut!(
                &mut things,
                header.thing_count as usize,
                Thing
            ))?;
            unsafe {
                things.set_len(header.thing_count as usize);
            }

            things
        };

        let things_properties = {
            let mut things_properties = Vec::<ThingProperties>::new();
            things_properties.reserve(header.thing_count as usize);
            file.read_exact(vec_as_byte_array_mut!(
                &mut things_properties,
                header.thing_count as usize,
                ThingProperties
            ))?;
            unsafe {
                things_properties.set_len(header.thing_count as usize);
            }

            things_properties
        };

        let layout = {
            let tile_count = (header.tile_width * header.tile_height) as usize;

            let mut layout = Vec::<LayoutObject>::new();
            layout.reserve(tile_count);
            file.read_exact(vec_as_byte_array_mut!(
                &mut layout,
                tile_count,
                LayoutObject
            ))?;
            unsafe {
                layout.set_len(tile_count);
            }

            layout
        };

        let layout_properties = {
            let tile_count = (header.tile_width * header.tile_height) as usize;

            let mut layout_properties = Vec::<LayoutObjectProperties>::new();
            layout_properties.reserve(tile_count);
            file.read_exact(vec_as_byte_array_mut!(
                &mut layout_properties,
                tile_count,
                LayoutObjectProperties
            ))?;
            unsafe {
                layout_properties.set_len(tile_count);
            }

            layout_properties
        };

        let checksum = {
            let mut checksum = core::mem::MaybeUninit::<[u8; 16]>::uninit();
            file.read_exact(as_byte_array_mut!(checksum.as_mut_ptr(), [u8; 16]))?;
            unsafe { checksum.assume_init() }
        };

        let this = Map {
            file_header,
            header,
            things,
            things_properties,
            layout,
            layout_properties,
        };

        assert_eq!(checksum, this.calculate_checksum());

        Ok(this)
    }

    pub fn save(&self, path: &str) -> Result<(), Box<dyn std::error::Error>> {
        let mut file = std::fs::OpenOptions::new()
            .read(true)
            .write(true)
            .truncate(true)
            .create(true)
            .open(path)?;

        file.write(as_byte_array!(&self.file_header, FileHeader))?;
        file.write(as_byte_array!(&self.header, Header))?;

        assert_eq!(self.header.thing_count as usize, self.things.len());
        file.write(vec_as_byte_array!(&self.things, Thing))?;

        assert_eq!(
            self.header.thing_count as usize,
            self.things_properties.len()
        );
        file.write(vec_as_byte_array!(&self.things_properties, ThingProperties))?;

        assert_eq!(
            (self.header.tile_width * self.header.tile_height) as usize,
            self.layout.len()
        );
        file.write(vec_as_byte_array!(&self.layout, LayoutObject))?;

        assert_eq!(
            (self.header.tile_width * self.header.tile_height) as usize,
            self.layout_properties.len()
        );
        file.write(vec_as_byte_array!(
            &self.layout_properties,
            LayoutObjectProperties
        ))?;

        file.write(&self.calculate_checksum())?;

        file.flush()?;

        Ok(())
    }

    fn calculate_checksum(&self) -> [u8; 16] {
        let headers_bytes = unsafe {
            core::slice::from_raw_parts(
                self as *const Self as *const u8,
                core::mem::size_of::<FileHeader>() + core::mem::size_of::<Header>(),
            )
        };
        let things_bytes = vec_as_byte_array!(&self.things, Thing);
        let things_properties_bytes = vec_as_byte_array!(&self.things_properties, ThingProperties);
        let layout_bytes = vec_as_byte_array!(&self.layout, LayoutObject);
        let layout_properties_bytes = vec_as_byte_array!(&self.layout_properties, LayoutObject);

        let mut checksum: [u16; 16] = [0; 16];
        for (i, byte) in md5::compute(headers_bytes).0.iter().enumerate() {
            checksum[i] = checksum[i].overflowing_add(*byte as u16).0;
        }
        for (i, byte) in md5::compute(things_bytes).0.iter().enumerate() {
            checksum[i] = checksum[i].overflowing_add(*byte as u16).0;
        }
        for (i, byte) in md5::compute(things_properties_bytes).0.iter().enumerate() {
            checksum[i] = checksum[i].overflowing_add(*byte as u16).0;
        }
        for (i, byte) in md5::compute(layout_bytes).0.iter().enumerate() {
            checksum[i] = checksum[i].overflowing_add(*byte as u16).0;
        }
        for (i, byte) in md5::compute(layout_properties_bytes).0.iter().enumerate() {
            checksum[i] = checksum[i].overflowing_add(*byte as u16).0;
        }

        let mut result = [0_u8; 16];
        for i in 0..result.len() {
            if i % 2 == 0 {
                result[i] = (checksum[i] >> 8) as u8;
            } else {
                result[i] = (checksum[i] & 0xFF) as u8;
            }
        }

        result
    }
}

#[cfg(test)]
mod test {
    use super::*;

    macro_rules! map_layout {
        ($layout: literal) => {
            $layout
                .split('\n')
                .map(|line| line.trim())
                .skip_while(|line| line.is_empty())
                .map(|line| {
                    let mut result = Vec::new();
                    result.reserve(line.as_bytes().len() / 6);

                    for (i, c) in line.as_bytes().iter().enumerate().step_by(6) {
                        const WALL_BYTES: &[u8] = "██".as_bytes();
                        const EMPTY_BYTES: &[u8] = "░░".as_bytes();

                        let char = &[
                            *c,
                            line.as_bytes()[i + 1],
                            line.as_bytes()[i + 2],
                            line.as_bytes()[i + 3],
                            line.as_bytes()[i + 4],
                            line.as_bytes()[i + 5],
                        ];
                        result.push(if char == WALL_BYTES {
                            LayoutObject::Wall
                        } else if char == EMPTY_BYTES {
                            LayoutObject::Empty
                        } else {
                            LayoutObject::Invalid
                        });
                    }

                    result
                })
                .collect::<Vec<Vec<LayoutObject>>>()
                .concat()
        };
    }

    #[test]
    fn demo_map() -> Result<(), Box<dyn std::error::Error>> {
        let map = Map {
            file_header: FileHeader {
                format: [b'K', b'M', b'A', b'P'],
                version: 0,
                revision: 1,
            },
            header: Header {
                tile_width: 16,
                tile_height: 16,
                px_per_tile: 32,
                thing_count: 1,
            },
            things: vec![Thing::Player],
            things_properties: vec![ThingProperties {
                px_width: 20,
                px_depth: 20,
                px_height: 20,
                _unused: 0,
                px_x_pos: 100,
                px_y_pos: 100,
                angle: core::f32::consts::PI / 2.0,
                resource_id: 7,
                hit_points: 100,
            }],
            layout: map_layout!(
                r#"
                ████████████████████████████████
                ██░░██░░░░░░░░░░░░░░██░░░░░░░░██
                ██░░██░░░░░░░░░░░░░░██░░░░░░░░██
                ██░░██░░░░░░░░░░░░░░██░░░░░░░░██
                ██░░░░░░░░░░░░░░░░░░░░░░░░░░░░██
                ██░░░░░░░░██░░░░░░░░░░░░░░██░░██
                ██░░░░░░░░░░░░░░░░░░░░░░░░░░░░██
                ██░░░░██████████████████░░██████
                ██░░░░██░░░░░░░░░░░░░░██░░░░░░██
                ██░░██░░░░░░░░░░░░░░██░░░░░░░░██
                ██░░██░░░░░░░░░░░░░░██░░░░░░░░██
                ██░░██░░░░██░░░░░░░░██░░░░██░░██
                ██░░░░░░░░██░░░░░░░░░░░░░░██░░██
                ██░░░░░░░░██░░░░░░░░░░░░░░██░░██
                ██░░░░░░░░░░░░░░░░░░░░░░░░░░░░██
                ████████████████████████████████
                "#
            ),
            layout_properties: Vec::from([LayoutObjectProperties::default(); 256]),
        };
        map.save("resources/e1m1.map")?;
        let read_back = Map::load("resources/e1m1.map")?;

        assert_eq!(map, read_back);

        Ok(())
    }
}
