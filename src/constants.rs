pub const PI: f32 = std::f32::consts::PI;
pub const PI_DIV_2: f32 = PI / 2.0;
pub const THREE_PI_DIV_2: f32 = (3.0 * PI) / 2.0;
pub const TWO_PI: f32 = 2.0 * PI;

pub const PLAYER_FOV_DEG: usize = 45;

pub const PROJECTED_COLUMN_WIDTH: usize = 2;

pub const MAIN_WINDOW_NO_SCALE_WIDTH: i32 =
    (PLAYER_FOV_DEG * RAYS_PER_DEGREE as usize * PROJECTED_COLUMN_WIDTH) as i32;
pub const MAIN_WINDOW_NO_SCALE_HEIGHT: i32 = (MAIN_WINDOW_NO_SCALE_WIDTH * 9) / 16;

pub const PROJECTION_SCALE_FACTOR: f32 = 1920_f32 / MAIN_WINDOW_NO_SCALE_WIDTH as f32;

pub const RAYS_PER_DEGREE: u8 = 10;
pub const RAY_ANGLE_INCREMENT: f32 = (1.0_f32 / RAYS_PER_DEGREE as f32) * (PI / 180.0);
