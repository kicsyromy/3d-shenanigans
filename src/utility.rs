pub trait FloatExtentions {
    fn normalize_angle(&self) -> f32;
}

impl FloatExtentions for f32 {
    fn normalize_angle(&self) -> f32 {
        use crate::constants::*;

        if *self > TWO_PI {
            *self - TWO_PI
        } else if *self < 0.0 {
            *self + TWO_PI
        } else {
            *self
        }
    }
}

pub fn round_up(v: f32) -> f32 {
    if (v - v.trunc()).abs() < f32::EPSILON {
        v
    } else {
        v.trunc() + 1.0
    }
}
