use std::{env, io::BufRead};

use cmake::Config;

const SDL2_VERSION: &str = "2.0.20";

fn main() {
    let build_path = Config::new("src")
        .define("BUILD_SHARED_LIBS", "ON")
        .define("PROJECT_ID", "nugget-rs")
        .define("SDL2_VERSION", SDL2_VERSION)
        .build_target("build-sdl2")
        .always_configure(false)
        .build();

    let build_path = format!("{}/build", build_path.to_str().unwrap());
    let sdl2_inc_dir = format!("{}/SDL2", build_path);

    let target = std::env::var("TARGET").unwrap();
    if target.contains("apple") {
    } else if target.contains("linux") {
    } else {
    }

    let mut bindings = bindgen::Builder::default()
        .use_core()
        .header(format!("{}/SDL.h", sdl2_inc_dir))
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .default_enum_style(bindgen::EnumVariation::ModuleConsts)
        .allowlist_type("^SDL.*")
        .allowlist_var("^SDL.*")
        .allowlist_function("^SDL.*")
        .layout_tests(false)
        .default_macro_constant_type(bindgen::MacroTypeVariation::Unsigned)
        .fit_macro_constants(false)
        .clang_arg(format!("-I{}", sdl2_inc_dir))
        .clang_arg("-DUSING_GENERATED_CONFIG_H")
        .clang_arg("-DSDL_MAIN_HANDLED");

    for line in std::io::BufReader::new(
        std::fs::File::options()
            .read(true)
            .write(false)
            .open(std::env::var("OUT_DIR").unwrap() + "/build/SDL2.cflags.txt")
            .unwrap(),
    )
    .lines()
    {
        let line = line.unwrap();
        bindings = bindings.clang_arg(line.as_str());
    }

    let bindings = bindings.generate().expect("Unable to generate bindings");
    let src_dir = format!("{}/src", env::var("CARGO_MANIFEST_DIR").unwrap());
    bindings
        .write_to_file(format!("{}/bindings.rs", src_dir))
        .expect("Couldn't write bindings!");

    for line in std::io::BufReader::new(
        std::fs::File::options()
            .read(true)
            .write(false)
            .open(std::env::var("OUT_DIR").unwrap() + "/build/SDL2.deps.txt")
            .unwrap(),
    )
    .lines()
    {
        let line = line.unwrap();
        let path = std::path::Path::new(&line);
        if path.exists() {
            let mut lib_name = path
                .file_stem()
                .unwrap()
                .to_str()
                .unwrap()
                .split(".so")
                .take(1)
                .next()
                .unwrap();
            if target.contains("linux") {
                lib_name = lib_name.trim_start_matches("lib");
            }
            let dir = path.parent().unwrap();
            println!(
                "cargo:rustc-link-search={}",
                dir.as_os_str().to_str().unwrap()
            );
            println!("cargo:rustc-link-lib={}", lib_name);
        } else {
            if line.starts_with("-L") {
                println!("cargo:rustc-link-search={}", &line[2..]);
            } else {
                println!("cargo:rustc-link-lib={}", line);
            }
        }
    }

    println!("cargo:rerun-if-changed={}/CMakeLists.txt", src_dir);
}
