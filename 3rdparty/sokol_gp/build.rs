use std::{env, error::Error, fs::File, io::Write};

use lazy_static::lazy_static;

const SOKOL_GFX_VERSION: &str = "a902d73c9109159bd041bbe6a14d3afe54629dfa";
lazy_static! {
    static ref SOKOL_GFX_HEADER_URL: String = {
        String::from("https://raw.githubusercontent.com/floooh/sokol/")
            + SOKOL_GFX_VERSION
            + "/sokol_gfx.h"
    };
}

const SOKOL_GP_VERSION: &str = "395e737284b9a4cf2f2d63b71d579839a21446bf";
lazy_static! {
    static ref SOKOL_GP_HEADER_URL: String = {
        String::from("https://raw.githubusercontent.com/edubart/sokol_gp/")
            + SOKOL_GP_VERSION
            + "/sokol_gp.h"
    };
}

const SOKOL_RENDER_BACKEND: &str = "SOKOL_GLCORE33";
const SOKOL_COMPILE_FLAGS: &[&str] = &["-DSOKOL_NO_DEPRECATED"];

fn download_to(url: &str, path: &str) -> Result<(), Box<dyn Error>> {
    let file_name = url.split('/').last().unwrap();
    let output_path = format!("{}/{}", path, file_name);

    if std::path::Path::new(&output_path).exists() {
        return Ok(());
    }

    let response = ureq::get(url).call()?;

    if response.status() != 200 {
        eprintln!(
            "Failed to download {}. HTTP status code {} {}",
            url,
            response.status(),
            response.status_text()
        );
    }

    let mut content = Vec::new();
    response.into_reader().read_to_end(&mut content)?;
    let mut file = File::create(&output_path)?;
    file.write_all(content.as_slice())?;
    file.flush()?;

    println!("cargo:rerun-if-changed={}", output_path);

    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let project_root_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    let out_dir = env::var("OUT_DIR").unwrap();
    download_to(&SOKOL_GFX_HEADER_URL, &out_dir)?;
    download_to(&SOKOL_GP_HEADER_URL, &out_dir)?;

    let mut build = cc::Build::new();
    for flag in SOKOL_COMPILE_FLAGS.iter() {
        if flag.starts_with("-D") {
            let kv: Vec<&str> = flag[2..].split('=').collect();
            build.define(kv[0], {
                if kv.len() > 1 {
                    Some(kv[1])
                } else {
                    None
                }
            });
        } else {
            build.flag(flag);
        }
    }
    build
        .include(&format!("{}", &out_dir))
        .define(SOKOL_RENDER_BACKEND, None)
        .file("src/sokol_gp.c")
        .compile("sokol_gp");
    println!("cargo:rustc-link-lib=static=sokol_gp");
    println!("cargo:rustc-link-lib=dylib=GL");

    {
        let mut file = File::create(String::from(&out_dir) + "/sokol_gp_bindings.h")?;
        file.write_all("#include <sokol_gfx.h>\n#include <sokol_gp.h>\n".as_bytes())?;
        file.flush()?;
    }

    let bindings = bindgen::Builder::default()
        .use_core()
        .header(format!("{}/sokol_gp_bindings.h", &out_dir))
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .allowlist_type("^sg_.*")
        .allowlist_type("^sgp_.*")
        .allowlist_var("^sg_.*")
        .allowlist_var("^sgp_.*")
        .allowlist_function("^sg_.*")
        .allowlist_function("^sgp_.*")
        .layout_tests(false)
        .default_enum_style(bindgen::EnumVariation::ModuleConsts)
        .default_macro_constant_type(bindgen::MacroTypeVariation::Unsigned)
        .fit_macro_constants(false)
        .clang_arg(format!("-I{}", out_dir))
        .clang_args(SOKOL_COMPILE_FLAGS.iter())
        .generate()
        .expect("Unable to generate bindings");

    bindings
        .write_to_file(format!("{}/src/bindings.rs", project_root_dir))
        .expect("Couldn't write bindings!");

    println!("cargo:rerun-if-changed={}/src/sokol_gp.c", project_root_dir);

    Ok(())
}
