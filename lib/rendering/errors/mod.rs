use std::fmt::{Debug, Display, Formatter};

pub use context_error::ContextError;

mod context_error;

#[derive(Debug, PartialEq)]
pub enum RenderingError {
    ContextError(ContextError),
}

impl From<ContextError> for RenderingError {
    fn from(e: ContextError) -> Self {
        RenderingError::ContextError(e)
    }
}

impl Display for RenderingError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ContextError(e) => Display::fmt(e, f),
        }
    }
}
