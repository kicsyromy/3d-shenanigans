use std::{
    cell::RefCell,
    ops::{Deref, DerefMut},
    rc::Rc,
};

use log::*;
use raw_window_handle::*;
use sdl2_sys::*;

use crate::events::*;
use crate::{
    rendering, Application, CallbackResult, DisableSend, DisableSync, NuggetResult, WindowError,
};

pub struct Window {
    data: Rc<RefCell<WindowData>>,
    _disable_send: Option<DisableSend>,
    _disable_sync: Option<DisableSync>,
}

impl Window {
    fn new(
        mut title: String,
        x: i32,
        y: i32,
        width: i32,
        height: i32,
        vsync: bool,
        key_press_callback: Box<dyn FnMut(&mut Window, &KeyPressEvent)>,
        key_release_callback: Box<dyn FnMut(&mut Window, &KeyReleaseEvent)>,
        mouse_move_callback: Box<dyn FnMut(&mut Window, &MouseMoveEvent)>,
        mouse_button_press_callback: Box<dyn FnMut(&mut Window, &MouseButtonPressEvent)>,
        mouse_button_release_callback: Box<dyn FnMut(&mut Window, &MouseButtonReleaseEvent)>,
        render_callback: Box<dyn FnMut(&mut Window, &RenderEvent)>,
    ) -> NuggetResult<Self> {
        title.push('\0');

        let mut this = Self {
            data: Rc::new(RefCell::new(WindowData {
                context: None,
                frame_counter: 0,
                time: 0_f32,
                key_press_callback,
                key_release_callback,
                mouse_move_callback,
                mouse_button_press_callback,
                mouse_button_release_callback,
                render_callback,
                handle: WindowHandle {
                    ptr: unsafe {
                        SDL_CreateWindow(
                            title.as_ptr() as *const i8,
                            x,
                            y,
                            width,
                            height,
                            (SDL_WindowFlags::SDL_WINDOW_RESIZABLE
                                | SDL_WindowFlags::SDL_WINDOW_OPENGL)
                                as Uint32,
                        )
                    },
                },
            })),
            _disable_send: None,
            _disable_sync: None,
        };

        if let Some(app) = Application::instance() {
            let mut app = app.write()?;

            {
                let new_context = rendering::Context::new(&mut this, vsync, app.deref_mut())?;
                let this_context = &mut this.data.borrow_mut().context;
                *this_context = Some(new_context);
            }

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &KeyPressEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(&mut this.key_press_callback, Box::new(|_, _| {}))
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.key_press_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &KeyReleaseEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(&mut this.key_release_callback, Box::new(|_, _| {}))
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.key_release_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &MouseMoveEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(&mut this.mouse_move_callback, Box::new(|_, _| {}))
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.mouse_move_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &MouseButtonPressEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(
                            &mut this.mouse_button_press_callback,
                            Box::new(|_, _| {}),
                        )
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.mouse_button_press_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &MouseButtonReleaseEvent| {
                    let mut cb = {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::replace(
                            &mut this.mouse_button_release_callback,
                            Box::new(|_, _| {}),
                        )
                    };

                    (cb)(
                        &mut Window {
                            data: Rc::clone(&this_data),
                            _disable_send: None,
                            _disable_sync: None,
                        },
                        event,
                    );

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.mouse_button_release_callback, &mut cb);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &MouseWheelEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_mouse_wheel(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowMouseEnterEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_mouse_entered(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowMouseLeaveEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_mouse_left(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowResizeEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_window_resized(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |window_id, event: &WindowCloseEvent| {
                    let mut this = RefCell::borrow_mut(this_data.deref());
                    this.on_window_closed(window_id, event);

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;

            let this_data = Rc::clone(&this.data);
            app.register_event_handler(
                move |_window_id, event: &RenderEvent| {
                    let (mut the_context, mut cb) = {
                        let mut this = RefCell::borrow_mut(this_data.deref());

                        this.time += event.time_elapsed();
                        if this.time >= 1.0 {
                            info!("FPS: {}", this.frame_counter + event.frames_rendered());
                            this.time -= 1.0;
                            this.frame_counter = -event.frames_rendered();
                        }

                        (
                            std::mem::replace(this.context.as_mut().unwrap(), Default::default()),
                            core::mem::replace(&mut this.render_callback, Box::new(|_, _| {})),
                        )
                    };

                    let mut window = Window {
                        data: Rc::clone(&this_data),
                        _disable_send: None,
                        _disable_sync: None,
                    };
                    the_context.activate().unwrap();
                    (cb)(&mut window, event);
                    the_context.present();

                    {
                        let mut this = RefCell::borrow_mut(this_data.deref());
                        core::mem::swap(&mut this.render_callback, &mut cb);
                        std::mem::swap(this.context.as_mut().unwrap(), &mut the_context);
                    }

                    CallbackResult::Continue
                },
                Some(this.id()),
                None,
            )?;
        } else {
            return Err(WindowError::NoApplicationInstance.into());
        }

        Ok(this)
    }

    pub fn id(&self) -> i32 {
        RefCell::borrow(self.data.deref()).id()
    }

    pub fn size(&self) -> (i32, i32) {
        RefCell::borrow(self.data.deref()).size()
    }

    pub fn width(&self) -> i32 {
        RefCell::borrow(self.data.deref()).width()
    }

    pub fn height(&self) -> i32 {
        RefCell::borrow(self.data.deref()).height()
    }

    pub fn capture_mouse(&mut self) {
        unsafe {
            SDL_SetRelativeMouseMode(SDL_bool::SDL_TRUE);
            SDL_SetWindowGrab(self.native_handle() as _, SDL_bool::SDL_TRUE);
        }
    }

    pub fn release_mouse(&mut self) {
        unsafe {
            SDL_SetRelativeMouseMode(SDL_bool::SDL_FALSE);
            SDL_SetWindowGrab(self.native_handle() as _, SDL_bool::SDL_FALSE);
        }
    }

    pub fn is_mouse_captured(&self) -> bool {
        unsafe { SDL_GetWindowGrab(self.native_handle_const() as _) == SDL_bool::SDL_TRUE }
    }

    pub fn native_handle(&mut self) -> *mut core::ffi::c_void {
        RefCell::borrow_mut(self.data.deref()).handle.ptr as _
    }

    fn native_handle_const(&self) -> *mut core::ffi::c_void {
        RefCell::borrow(self.data.deref()).handle.ptr as _
    }
}

struct WindowData {
    context: Option<rendering::Context>,
    frame_counter: i64,
    time: f32,
    key_press_callback: Box<dyn FnMut(&mut Window, &KeyPressEvent)>,
    key_release_callback: Box<dyn FnMut(&mut Window, &KeyReleaseEvent)>,
    mouse_move_callback: Box<dyn FnMut(&mut Window, &MouseMoveEvent)>,
    mouse_button_press_callback: Box<dyn FnMut(&mut Window, &MouseButtonPressEvent)>,
    mouse_button_release_callback: Box<dyn FnMut(&mut Window, &MouseButtonReleaseEvent)>,
    render_callback: Box<dyn FnMut(&mut Window, &RenderEvent)>,
    handle: WindowHandle,
}

impl WindowData {
    pub fn id(&self) -> i32 {
        (unsafe { SDL_GetWindowID(self.handle.ptr) }) as i32
    }

    pub fn size(&self) -> (i32, i32) {
        let mut w = 0_i32;
        let mut h = 0_i32;
        unsafe {
            SDL_GetWindowSize(self.handle.ptr, &mut w, &mut h);
        }
        (w, h)
    }

    pub fn width(&self) -> i32 {
        let mut w = 0_i32;
        unsafe {
            SDL_GetWindowSize(self.handle.ptr, &mut w, std::ptr::null_mut());
        }
        w
    }

    pub fn height(&self) -> i32 {
        let mut h = 0_i32;
        unsafe {
            SDL_GetWindowSize(self.handle.ptr, std::ptr::null_mut(), &mut h);
        }
        h
    }

    #[allow(dead_code)]
    pub fn vsync(&self) -> bool {
        self.context.as_ref().unwrap().vsync()
    }

    #[allow(dead_code)]
    pub fn set_vsync(&mut self, value: bool) -> NuggetResult<()> {
        self.context.as_mut().unwrap().set_vsync(value)
    }

    fn on_mouse_wheel(&mut self, window_id: i32, event: &MouseWheelEvent) {
        trace!("Mouse wheel {:#?} on window {}", event, window_id);
    }

    fn on_mouse_entered(&mut self, window_id: i32, event: &WindowMouseEnterEvent) {
        trace!("Mouse entered {:#?} on window {}", event, window_id);
    }

    fn on_mouse_left(&mut self, window_id: i32, event: &WindowMouseLeaveEvent) {
        trace!("Mouse left {:#?} on window {}", event, window_id);
    }

    fn on_window_resized(&mut self, window_id: i32, event: &WindowResizeEvent) {
        trace!("Window resized {:#?} on window {}", event, window_id);
    }

    fn on_window_closed(&mut self, window_id: i32, event: &WindowCloseEvent) {
        trace!("Window closed {:#?} pressed on window {}", event, window_id);
        unsafe { SDL_DestroyWindow(self.handle.ptr) };
    }
}

pub struct WindowBuilder {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
    title: String,
    vsync: bool,
    key_press_callback: Box<dyn FnMut(&mut Window, &KeyPressEvent)>,
    key_release_callback: Box<dyn FnMut(&mut Window, &KeyReleaseEvent)>,
    mouse_move_callback: Box<dyn FnMut(&mut Window, &MouseMoveEvent)>,
    mouse_button_press_callback: Box<dyn FnMut(&mut Window, &MouseButtonPressEvent)>,
    mouse_button_release_callback: Box<dyn FnMut(&mut Window, &MouseButtonReleaseEvent)>,
    render_callback: Box<dyn FnMut(&mut Window, &RenderEvent)>,
}

impl WindowBuilder {
    pub fn new() -> Self {
        Self {
            x: SDL_WINDOWPOS_UNDEFINED_MASK as i32,
            y: SDL_WINDOWPOS_UNDEFINED_MASK as i32,
            width: 1280,
            height: 800,
            title: "".into(),
            vsync: true,
            key_press_callback: Box::new(|_, _| {}),
            key_release_callback: Box::new(|_, _| {}),
            mouse_move_callback: Box::new(|_, _| {}),
            mouse_button_press_callback: Box::new(|_, _| {}),
            mouse_button_release_callback: Box::new(|_, _| {}),
            render_callback: Box::new(|_, _| {}),
        }
    }

    pub fn title(mut self, v: String) -> Self {
        self.title = v;

        self
    }

    pub fn x(mut self, v: i32) -> Self {
        self.x = v;

        self
    }

    pub fn y(mut self, v: i32) -> Self {
        self.y = v;

        self
    }

    pub fn width(mut self, v: i32) -> Self {
        self.width = v;

        self
    }

    pub fn height(mut self, v: i32) -> Self {
        self.height = v;

        self
    }

    pub fn vsync(mut self, v: bool) -> Self {
        self.vsync = v;

        self
    }

    pub fn on_key_pressed<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &KeyPressEvent) + 'static,
    {
        self.key_press_callback = Box::new(cb);

        self
    }

    pub fn on_key_released<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &KeyReleaseEvent) + 'static,
    {
        self.key_release_callback = Box::new(cb);

        self
    }

    pub fn on_mouse_moved<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &MouseMoveEvent) + 'static,
    {
        self.mouse_move_callback = Box::new(cb);

        self
    }

    pub fn on_mouse_button_pressed<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &MouseButtonPressEvent) + 'static,
    {
        self.mouse_button_press_callback = Box::new(cb);

        self
    }

    pub fn on_mouse_button_released<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &MouseButtonReleaseEvent) + 'static,
    {
        self.mouse_button_release_callback = Box::new(cb);

        self
    }

    pub fn on_render<F>(mut self, cb: F) -> Self
    where
        F: FnMut(&mut Window, &RenderEvent) + 'static,
    {
        self.render_callback = Box::new(cb);

        self
    }

    pub fn build(self) -> NuggetResult<Window> {
        Window::new(
            self.title,
            self.x,
            self.y,
            self.width,
            self.height,
            self.vsync,
            self.key_press_callback,
            self.key_release_callback,
            self.mouse_move_callback,
            self.mouse_button_press_callback,
            self.mouse_button_release_callback,
            self.render_callback,
        )
    }
}

pub(crate) struct WindowHandle {
    pub(crate) ptr: *mut SDL_Window,
}

unsafe impl HasRawWindowHandle for WindowHandle {
    fn raw_window_handle(&self) -> RawWindowHandle {
        let mut wmi: SDL_SysWMinfo = unsafe { core::mem::zeroed() };

        unsafe { SDL_GetVersion(&mut wmi.version) };

        if unsafe { SDL_GetWindowWMInfo(self.ptr, &mut wmi) } == SDL_bool::SDL_FALSE {
            panic!("Failed to get platform windowing information");
        }

        match wmi.subsystem {
            SDL_SYSWM_TYPE::SDL_SYSWM_X11 => {
                #[cfg(target_os = "linux")]
                {
                    let mut handle = XlibHandle::empty();
                    handle.display = unsafe { wmi.info.x11.display } as *mut _;
                    handle.window = unsafe { wmi.info.x11.window };

                    RawWindowHandle::Xlib(handle)
                }
            }
            SDL_SYSWM_TYPE::SDL_SYSWM_WAYLAND => {
                #[cfg(target_os = "linux")]
                {
                    let mut handle = WaylandHandle::empty();
                    handle.display = unsafe { wmi.info.wl.display } as *mut _;
                    handle.surface = unsafe { wmi.info.wl.surface } as *mut _;

                    RawWindowHandle::Wayland(handle)
                }
            }
            SDL_SYSWM_TYPE::SDL_SYSWM_WINDOWS => {
                #[cfg(target_os = "windows")]
                {
                    let mut handle = Win32Handle::empty();
                    handle.hwnd = unsafe { wmi.info.win.hwnd };
                    handle.hinstance = unsafe { wmi.info.win.hinstance };

                    RawWindowHandle::Win32(handle)
                }

                #[cfg(not(target_os = "windows"))]
                unreachable!()
            }
            SDL_SYSWM_TYPE::SDL_SYSWM_WINRT => {
                #[cfg(target_os = "windows")]
                {
                    let mut handle = WinRtHandle::empty();
                    handle.core_window = unsafe { wmi.info.winrt.window };

                    RawWindowHandle::WinRt(handle)
                }

                #[cfg(not(target_os = "windows"))]
                unreachable!()
            }
            SDL_SYSWM_TYPE::SDL_SYSWM_ANDROID => {
                #[cfg(target_os = "android")]
                {
                    let mut handle = AndroidNdkHandle::empty();
                    handle.a_native_window = unsafe { wmi.info.android.window };

                    RawWindowHandle::AndroidNdk(handle)
                }

                #[cfg(not(target_os = "android"))]
                unreachable!()
            }
            SDL_SYSWM_TYPE::SDL_SYSWM_COCOA => {
                #[cfg(target_os = "macos")]
                {
                    let mut handle = AppKitHandle::empty();
                    handle.ns_window = unsafe { wmi.info.cocoa.window };

                    RawWindowHandle::AppKit(handle)
                }

                #[cfg(not(target_os = "macos"))]
                unreachable!()
            }
            SDL_SYSWM_TYPE::SDL_SYSWM_UIKIT => {
                #[cfg(target_os = "ios")]
                {
                    let mut handle = UiKitHandle::empty();
                    handle.ui_window = unsafe { wmi.info.uikit.window };

                    RawWindowHandle::UiKit(handle)
                }

                #[cfg(not(target_os = "ios"))]
                unreachable!()
            }
            _ => panic!("Unsupported video subsystem"),
        }
    }
}

impl Drop for WindowHandle {
    fn drop(&mut self) {
        unsafe { SDL_DestroyWindow(self.ptr) }
    }
}
