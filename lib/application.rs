/// Application provides the main application event loop, and it is the primary responsible of
/// handling operating system events (input, focus, etc.).
use std::{
    collections::{HashMap, HashSet},
    marker::PhantomData,
    ops::{Deref, DerefMut},
    sync::{
        atomic::{AtomicUsize, Ordering},
        Arc, Mutex, RwLock, Weak,
    },
};

use log::*;
use once_cell::sync::Lazy;
use sdl2_sys::*;

use crate::{events, events::*, ApplicationError, NuggetResult};

static APP_INSTANCE: Lazy<Mutex<Option<Weak<RwLock<Box<Application>>>>>> =
    Lazy::new(|| Mutex::new(None));

/// Action to take after executing a callback
#[derive(PartialEq)]
pub enum CallbackResult {
    /// Continue executing callbacks after this one finishes
    Continue,
    /// Break after this callback finishes execution
    Break,
}

/// Used by programs to provide their event loop.
///
/// A program must contain exactly one instance of Application. Attempting to
/// create more more than one instance of this class will result in
/// an error code of [AlreadyExists](ApplicationError::AlreadyExists) being
/// returned.
///
/// Application provides the main application event loop, and it is the primary responsible of
/// handling operating system events (input, focus, etc.).
///
/// Once a valid instance is created the main loop can be started using the
/// [exec()](Application::exec) function.
///
/// A convenience static function, [instance](Application::instance), is provided
/// in order to obtain the active instance of Application in the program.
///
/// # Examples
/// ```
/// fn main() -> Result<(), Box<dyn std::error::Error>> {
///     use nugget_rs::Application;
///     let app = Application::new()?;
///
///     // Your application code goes here
///     # app.write().unwrap().post_callback(|| Application::instance().unwrap().write().unwrap().quit()).unwrap();
///
///     Application::exec(app).map_err(|e| Box::new(e).into())
/// }
/// ```
pub struct Application<'a> {
    clients: [Vec<EventClient>; EVENT_TYPE_COUNT],
    event_handler_id_counter: AtomicUsize,
    event_handler_ids: HashMap<usize, EventType>,

    callback_event_id: u32,

    quit: bool,

    _phantom: PhantomData<&'a u8>,
}

impl<'a> Application<'a> {
    /// Create a new Application instance.
    pub fn new() -> NuggetResult<Arc<RwLock<Box<Application<'static>>>>> {
        let mut self_instance = APP_INSTANCE.lock().unwrap();

        if self_instance.is_some() {
            return Err(ApplicationError::AlreadyExists.into());
        }

        let sdl_init_result = unsafe {
            SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK | SDL_INIT_EVENTS | SDL_INIT_TIMER)
        };
        if sdl_init_result < 0 {
            let error_string = format!("Failed to initialize SDL: {}", sdl_error!());
            return Err(ApplicationError::SDLInitializationFailed(error_string).into());
        }

        let sdl_gl_init_result = unsafe {
            SDL_GL_SetAttribute(
                SDL_GLattr::SDL_GL_CONTEXT_PROFILE_MASK,
                SDL_GLprofile::SDL_GL_CONTEXT_PROFILE_CORE as i32,
            )
        };
        if sdl_gl_init_result < 0 {
            let error_string = format!("Failed to initialize SDL: {}", sdl_error!());
            return Err(ApplicationError::SDLInitializationFailed(error_string).into());
        }

        let sdl_gl_init_result =
            unsafe { SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_MAJOR_VERSION, 3) };
        if sdl_gl_init_result < 0 {
            let error_string = format!("Failed to initialize SDL: {}", sdl_error!());
            return Err(ApplicationError::SDLInitializationFailed(error_string).into());
        }

        let sdl_gl_init_result =
            unsafe { SDL_GL_SetAttribute(SDL_GLattr::SDL_GL_CONTEXT_MINOR_VERSION, 3) };
        if sdl_gl_init_result < 0 {
            let error_string = format!("Failed to initialize SDL: {}", sdl_error!());
            return Err(ApplicationError::SDLInitializationFailed(error_string).into());
        }

        let callback_event_id = unsafe { SDL_RegisterEvents(1) };
        if callback_event_id == u32::MAX {
            let error_string =
                "Failed to initialize SDL: Cannot initialize custom event infrastructure".into();
            return Err(ApplicationError::SDLInitializationFailed(error_string).into());
        }

        const EVENT_CLIENT_VEC: Vec<EventClient> = Vec::new();
        let this = Arc::new(RwLock::new(Box::new(Application {
            clients: [EVENT_CLIENT_VEC; EVENT_TYPE_COUNT],
            event_handler_id_counter: AtomicUsize::new(0),
            event_handler_ids: HashMap::new(),
            callback_event_id,
            quit: false,
            _phantom: PhantomData,
        })));

        core::mem::swap(self_instance.deref_mut(), &mut Some(Arc::downgrade(&this)));

        Ok(this)
    }

    /// Post a custom event to the main application event loop.
    ///
    /// The function takes a [trait@FnOnce] to call on the main application thread.
    ///
    /// # Examples:
    /// ```
    /// use nugget_rs::*;
    /// use nugget_rs::events::*;
    ///
    /// let app = Application::new().unwrap();
    ///
    /// app.write().unwrap().post_callback(|| {
    ///     println!(
    ///         "This is a message printed from the thread that initialized the Application instance"
    ///     );
    ///     # Application::instance().unwrap().write().unwrap().quit();
    /// }).unwrap();
    ///
    /// Application::exec(app).unwrap()
    /// ```
    pub fn post_callback<F>(&self, f: F) -> NuggetResult<()>
    where
        F: FnOnce(),
        F: Sync + 'a,
    {
        let mut event: SDL_Event = unsafe { core::mem::zeroed() };
        event.type_ = self.callback_event_id;
        event.user.code = events::EventType::User as i32;

        let fat_ptr: [usize; 2] =
            unsafe { core::mem::transmute(Box::leak(Box::new(f)) as *mut dyn FnOnce()) };
        event.user.data1 = fat_ptr[0] as _;
        event.user.data2 = fat_ptr[1] as _;

        let result = unsafe { SDL_PushEvent(&mut event) };
        if result < 0 {
            return Err(ApplicationError::PostEventFailed(sdl_error!().into()).into());
        } else if result == 0 {
            return Err(ApplicationError::PostEventFailed("The event was filtered".into()).into());
        }

        Ok(())
    }

    /// Register a callable as an [event](events::Event) handler.
    ///
    /// # Parameters:
    ///  - **F** A [trait@FnMut] that serves as the event handler
    ///  - **client_id** The id of the client that handles the event (optional)
    ///  - **priority** The priority with which to call the event handler; higher priorities have lower values (optional)
    ///
    /// # Callback Parameters
    ///  - **id** The client_id passed to register_event_handler
    ///  - [**EventType**](events::EventType) the event to subscribe to
    ///
    /// # Examples:
    /// ```
    /// use nugget_rs::*;
    /// use nugget_rs::events::*;
    ///
    /// let app = Application::new().unwrap();
    ///
    /// app.write().unwrap().register_event_handler(
    ///     |_, e: &KeyPressEvent| {
    ///         // Handle application-wide key press events
    ///         // ...
    ///         CallbackResult::Continue
    ///     },
    ///     None,
    ///     None,
    /// ).unwrap();
    ///
    /// # app.write().unwrap().post_callback(|| Application::instance().unwrap().write().unwrap().quit()).unwrap();
    ///
    /// Application::exec(app).unwrap()
    /// ```
    pub fn register_event_handler<EventType, F>(
        &mut self,
        callback: F,
        client_id: Option<i32>,
        priority: Option<i32>,
    ) -> NuggetResult<usize>
    where
        EventType: Event + 'a,
        F: FnMut(i32, &EventType) -> CallbackResult + 'a,
    {
        let client_id = client_id.map_or(-1, |id| id);
        let priority = priority.map_or(0, |p| p);

        let event_handler_id = self.event_handler_id_counter.fetch_add(1, Ordering::SeqCst);
        self.event_handler_ids
            .insert(event_handler_id, EventType::event_type());

        let event_clients = &mut self.clients[EventType::event_type() as usize];
        event_clients.push(EventClient {
            event_handler_id,
            client_id,
            priority,
            f: |id, event, callback| {
                let event: &EventType = match event {
                    GenericHandle::Ptr(ptr) => unsafe { core::mem::transmute(ptr) },
                    GenericHandle::PtrMut(_) => panic!(),
                };

                let callback: &mut F = match callback {
                    GenericHandle::PtrMut(ptr) => unsafe { core::mem::transmute(ptr) },
                    GenericHandle::Ptr(_) => panic!(),
                };

                callback(id, event)
            },
            callback: (Box::leak(Box::new(callback)) as *mut _ as *mut core::ffi::c_void).into(),
            callback_deleter: |callback| {
                let callback: *mut F = match callback {
                    GenericHandle::PtrMut(ptr) => unsafe { core::mem::transmute(ptr) },
                    GenericHandle::Ptr(_) => panic!(),
                };
                let b = unsafe { Box::from_raw(callback) };
                drop(b);
            },
        });
        event_clients.sort_unstable_by(|lhs, rhs| lhs.priority.cmp(&rhs.priority));

        Ok(event_handler_id)
    }

    /// Starts the main application loop.
    ///
    /// This function consumes the application instance and blocks the calling thread until the
    /// event loop is stopped.
    #[allow(non_upper_case_globals)]
    pub fn exec(app: Arc<RwLock<Box<Self>>>) -> NuggetResult<()> {
        use detail::*;

        let mut open_windows = HashSet::new();
        let mut frames_rendered_counters: HashMap<u32, i64> = HashMap::new();

        unsafe {
            use sokol_gp_sys::*;

            let sgdesc: sg_desc = core::mem::zeroed();

            sg_setup(&sgdesc);
            if !sg_isvalid() {
                return Err(ApplicationError::GFXInitializationFailed(
                    "Failed to initialize sokol_gfx".into(),
                )
                .into());
            }

            let sgpdesc: sgp_desc = core::mem::zeroed();
            sgp_setup(&sgpdesc);
            if !sgp_is_valid() {
                return Err(ApplicationError::GFXInitializationFailed(
                    "Failed to initialize sokol_gp".into(),
                )
                .into());
            }
        }

        let key_states = unsafe {
            core::slice::from_raw_parts(SDL_GetKeyboardState(std::ptr::null_mut()), KEY_CODE_COUNT)
        };
        let mut mouse_button_states = [0; MOUSE_BUTTON_COUNT];
        let mut mouse_button_pressed = false;

        // Se up some placeholders, where different elements of the Application instance
        // can be swapped out to, if needed by a particular event.
        // Swapping is done as to allow event handlers to take a mutable borrow of the
        // Application instance.
        let mut event_clients = Vec::new();

        info!("Starting application execution");

        let mut quit = lock(&app)?.quit;
        let callback_event_id = lock(&app)?.callback_event_id;

        let mut last_instant = std::time::Instant::now();
        let mut event: SDL_Event = unsafe { core::mem::zeroed() };
        'main_loop: while !quit {
            while unsafe { SDL_PollEvent(&mut event) } != 0 {
                if unsafe { event.type_ } == SDL_EventType::SDL_QUIT as u32 {
                    break 'main_loop;
                }

                if unsafe { event.type_ } == callback_event_id {
                    let user_code = unsafe { event.user.code };
                    if user_code != events::EventType::User as i32 {
                        error!(
                            "Callback event has invalid code; expected {} but got {}.",
                            events::EventType::User as i32,
                            user_code
                        );
                        continue;
                    }

                    let fat_ptr: [usize; 2] =
                        unsafe { [event.user.data1 as _, event.user.data2 as _] };
                    let user_cb_ptr: *mut dyn FnOnce() = unsafe { core::mem::transmute(fat_ptr) };
                    let user_cb = unsafe { Box::from_raw(user_cb_ptr) };
                    user_cb();

                    continue;
                }

                let event_type = unsafe { event.type_ } as SDL_EventType::Type;
                match event_type {
                    SDL_EventType::SDL_WINDOWEVENT => {
                        let window_id = unsafe { event.window.windowID };

                        let window_event_type =
                            unsafe { event.window.event } as SDL_WindowEventID::Type;
                        match window_event_type {
                            SDL_WindowEventID::SDL_WINDOWEVENT_SHOWN => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowShown as usize],
                                    );
                                }

                                trace!("Window show event; window id: {}", window_id);

                                if !open_windows.contains(&window_id) {
                                    open_windows.insert(window_id);
                                    frames_rendered_counters.insert(window_id, 0);
                                }

                                let window_event =
                                    WindowShowEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowShown as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_EXPOSED => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowShown as usize],
                                    );
                                }

                                trace!("Window show event; window id: {}", window_id);

                                if !open_windows.contains(&window_id) {
                                    open_windows.insert(window_id);
                                    frames_rendered_counters.insert(window_id, 0);
                                }

                                let window_event =
                                    WindowShowEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowShown as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_HIDDEN => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowHidden as usize],
                                    );
                                }

                                trace!("Window hide event; window id: {}", window_id);

                                let window_event =
                                    WindowHideEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowHidden as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_MOVED => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowMoved as usize],
                                    );
                                }

                                let (x, y) = unsafe { (event.window.data1, event.window.data2) };
                                trace!(
                                    "Window move event ({}, {}); window id: {}",
                                    x,
                                    y,
                                    window_id
                                );

                                let window_event =
                                    WindowMoveEvent::new(x, y, &key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowMoved as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_RESIZED => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowResized as usize],
                                    );
                                }

                                let (width, height) =
                                    unsafe { (event.window.data1, event.window.data2) };
                                trace!(
                                    "Window resize event ({}, {}); window id: {}",
                                    width,
                                    height,
                                    window_id
                                );

                                let window_event = WindowResizeEvent::new(
                                    width,
                                    height,
                                    &key_states,
                                    &mouse_button_states,
                                );
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowResized as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_SIZE_CHANGED => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_MINIMIZED => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_MAXIMIZED => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_RESTORED => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_ENTER => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowMouseEntered as usize],
                                    );
                                }

                                trace!("Window mouse enter event; window id: {}", window_id);

                                let window_event =
                                    WindowMouseEnterEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowMouseEntered as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_LEAVE => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowMouseLeft as usize],
                                    );
                                }

                                trace!("Window mouse leave event; window id: {}", window_id);

                                let window_event =
                                    WindowMouseLeaveEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowMouseLeft as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_FOCUS_GAINED => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowGainedFocus as usize],
                                    );
                                }

                                trace!("Window focus gained event; window id: {}", window_id);

                                let window_event =
                                    WindowGainFocusEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowGainedFocus as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_FOCUS_LOST => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowLostFocus as usize],
                                    );
                                }

                                trace!("Window focus lost event; window id: {}", window_id);

                                let window_event =
                                    WindowLooseFocusEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowLostFocus as usize],
                                        &mut event_clients,
                                    );
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_CLOSE => {
                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut event_clients,
                                        &mut this.clients[EventType::WindowClosed as usize],
                                    );
                                }

                                trace!("Window close event; window id: {}", window_id);

                                let window_event =
                                    WindowCloseEvent::new(&key_states, &mouse_button_states);
                                send_event(window_id, window_event, &mut event_clients);

                                {
                                    let mut this = lock(&app)?;
                                    core::mem::swap(
                                        &mut this.clients[EventType::WindowClosed as usize],
                                        &mut event_clients,
                                    );

                                    // Workaround to stop crashes when trying to render a destroyed window
                                    let mut remove_index = None;
                                    for (index, client) in
                                        this.clients[EventType::Render as usize].iter().enumerate()
                                    {
                                        if client.client_id == window_id as i32 {
                                            remove_index = Some(index);
                                        }
                                    }
                                    if remove_index.is_some() {
                                        this.clients[EventType::Render as usize]
                                            .remove(remove_index.unwrap());
                                    }
                                }

                                open_windows.remove(&window_id);
                                frames_rendered_counters.remove(&window_id);

                                if open_windows.is_empty() {
                                    break 'main_loop;
                                }
                            }
                            SDL_WindowEventID::SDL_WINDOWEVENT_TAKE_FOCUS => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_HIT_TEST => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_ICCPROF_CHANGED => {}
                            SDL_WindowEventID::SDL_WINDOWEVENT_DISPLAY_CHANGED => {
                                trace!(
                                    "Window display changed event; window id: {}; new display: {}",
                                    window_id,
                                    unsafe { event.window.data1 }
                                );
                            }
                            _ => {}
                        }
                    }
                    SDL_EventType::SDL_MOUSEMOTION => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::MouseMoved as usize],
                            );
                        }

                        let mouse_pressed = if mouse_button_pressed {
                            SDL_bool::SDL_TRUE
                        } else {
                            SDL_bool::SDL_FALSE
                        };
                        if unsafe { SDL_CaptureMouse(mouse_pressed) } < 0 {
                            warn!(
                                "Failed to {} mouse capture outside of the window: {}",
                                if mouse_button_pressed {
                                    "enable"
                                } else {
                                    "disable"
                                },
                                sdl_error!()
                            );
                        }

                        let window_id = unsafe { event.motion.windowID };
                        let (x, y, rel_x, rel_y) = unsafe {
                            (
                                event.motion.x,
                                event.motion.y,
                                event.motion.xrel,
                                event.motion.yrel,
                            )
                        };

                        trace!("Mouse move event; ({}, {})", x, y);

                        let mouse_move_event = MouseMoveEvent::new(
                            x,
                            y,
                            rel_x,
                            rel_y,
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, mouse_move_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::MouseMoved as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_FINGERDOWN => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::MouseButtonPressed as usize],
                            );
                        }

                        let window_id = unsafe { event.tfinger.windowID };
                        let (x, y) = unsafe { (event.tfinger.x as i32, event.tfinger.y as i32) };
                        let button = 0;

                        trace!("Finger press event; ({}, {}); button: {}", x, y, button);

                        mouse_button_pressed = true;
                        mouse_button_states[button as usize] = 1;
                        let mouse_button_event = MouseButtonPressEvent::new(
                            x,
                            y,
                            button,
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, mouse_button_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::MouseButtonPressed as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_MOUSEBUTTONDOWN => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::MouseButtonPressed as usize],
                            );
                        }

                        let window_id = unsafe { event.button.windowID };
                        let (x, y) = unsafe { (event.button.x, event.button.y) };
                        let button = unsafe { event.button.button };

                        trace!(
                            "Mouse button press event; ({}, {}); button: {}",
                            x,
                            y,
                            button
                        );

                        mouse_button_pressed = true;
                        mouse_button_states[button as usize] = 1;
                        let mouse_button_event = MouseButtonPressEvent::new(
                            x,
                            y,
                            button,
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, mouse_button_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::MouseButtonPressed as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_FINGERUP => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::MouseButtonReleased as usize],
                            );
                        }

                        let window_id = unsafe { event.tfinger.windowID };
                        let (x, y) = unsafe { (event.tfinger.x as i32, event.tfinger.y as i32) };
                        let button = 0;

                        trace!("Finger release event; ({}, {}); button: {}", x, y, button);

                        mouse_button_pressed = false;
                        mouse_button_states[button as usize] = 0;
                        let mouse_button_event = MouseButtonReleaseEvent::new(
                            x,
                            y,
                            button,
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, mouse_button_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::MouseButtonReleased as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_MOUSEBUTTONUP => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::MouseButtonReleased as usize],
                            );
                        }

                        let window_id = unsafe { event.button.windowID };
                        let (x, y) = unsafe { (event.button.x, event.button.y) };
                        let button = unsafe { event.button.button };

                        trace!(
                            "Mouse button release event; ({}, {}); button: {}",
                            x,
                            y,
                            button
                        );

                        mouse_button_pressed = false;
                        mouse_button_states[button as usize] = 0;
                        let mouse_button_event = MouseButtonReleaseEvent::new(
                            x,
                            y,
                            button,
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, mouse_button_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::MouseButtonReleased as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_MOUSEWHEEL => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::MouseWheelTurned as usize],
                            );
                        }

                        let window_id = unsafe { event.wheel.windowID };
                        let (x, y) = unsafe { (event.wheel.x, event.wheel.y) };
                        let flipped = unsafe {
                            event.wheel.direction
                                == SDL_MouseWheelDirection::SDL_MOUSEWHEEL_FLIPPED as u32
                        };

                        trace!("Mouse wheel event; ({}, {}); flipped {}", x, y, flipped);

                        let wheel_event =
                            MouseWheelEvent::new(x, y, flipped, &key_states, &mouse_button_states);
                        send_event(window_id, wheel_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::MouseWheelTurned as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_KEYUP => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::KeyReleased as usize],
                            );
                        }

                        let window_id = unsafe { event.key.windowID };
                        let scancode = unsafe { event.key.keysym.scancode };

                        trace!("Key release event; scancode: {}", scancode);

                        let key_event = KeyReleaseEvent::new(
                            scancode.try_into().unwrap(),
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, key_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::KeyReleased as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    SDL_EventType::SDL_KEYDOWN => {
                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut event_clients,
                                &mut this.clients[EventType::KeyPressed as usize],
                            );
                        }

                        let window_id = unsafe { event.key.windowID };
                        let scancode = unsafe { event.key.keysym.scancode };

                        trace!("Key press event; scancode: {}", scancode);

                        let key_event = KeyPressEvent::new(
                            scancode.try_into().unwrap(),
                            &key_states,
                            &mouse_button_states,
                        );
                        send_event(window_id, key_event, &mut event_clients);

                        {
                            let mut this = lock(&app)?;
                            core::mem::swap(
                                &mut this.clients[EventType::KeyPressed as usize],
                                &mut event_clients,
                            );
                        }
                    }
                    _ => {}
                }
            }

            {
                let mut this = lock(&app)?;
                core::mem::swap(
                    &mut event_clients,
                    &mut this.clients[EventType::Render as usize],
                );
            }

            let elapsed = last_instant.elapsed().as_secs_f32();
            last_instant = std::time::Instant::now();

            for window_id in &open_windows {
                let frames_rendered_for_window =
                    frames_rendered_counters.get_mut(window_id).unwrap();
                send_event(
                    *window_id,
                    RenderEvent::new(
                        elapsed,
                        *frames_rendered_for_window,
                        &key_states,
                        &mouse_button_states,
                    ),
                    &mut event_clients,
                );
                *frames_rendered_for_window += 1;
            }

            {
                let mut this = lock(&app)?;
                core::mem::swap(
                    &mut this.clients[EventType::Render as usize],
                    &mut event_clients,
                );
            }

            quit = lock(&app)?.quit;
        }

        Ok(())
    }

    pub fn quit(&mut self) {
        self.quit = true;
    }

    /// Returns the unique instance of the running Application.
    ///
    /// If an Application instance was never created, or the application loop has exited then
    /// returns None.
    pub fn instance() -> Option<Arc<RwLock<Box<Application<'static>>>>> {
        let this = APP_INSTANCE.lock();
        if this.is_err() {
            return None;
        }

        let this = this.unwrap();
        match this.deref() {
            Some(app) => app.upgrade(),
            None => None,
        }
    }
}

impl Drop for Application<'_> {
    fn drop(&mut self) {
        use sokol_gp_sys::*;

        let mut self_instance = APP_INSTANCE.lock().unwrap();
        if self_instance.is_some() {
            unsafe {
                SDL_Quit();
                sgp_shutdown();
                sg_shutdown();
            }

            core::mem::swap(self_instance.deref_mut(), &mut None);
        }
    }
}

type CallbackType = fn(i32, GenericHandle, GenericHandle) -> CallbackResult;

pub(crate) struct EventClient {
    #[allow(dead_code)]
    event_handler_id: usize,
    client_id: i32,
    priority: i32,
    f: CallbackType,
    callback: GenericHandle,
    callback_deleter: fn(GenericHandle),
}

impl EventClient {
    pub fn call<'a>(&mut self, event: &'a (impl Event + 'a)) -> CallbackResult {
        let event = event as *const _ as *const core::ffi::c_void;
        let callback = self.callback;

        if !callback.is_null() {
            (self.f)(self.client_id, event.into(), callback)
        } else {
            CallbackResult::Continue
        }
    }
}

impl Drop for EventClient {
    fn drop(&mut self) {
        (self.callback_deleter)(self.callback);
    }
}

// FIXME: Hack to enable raw pointers
//        This is a quick and dirty way to do unsafe type erasure
#[derive(Copy, Clone)]
enum GenericHandle {
    Ptr(*const core::ffi::c_void),
    PtrMut(*mut core::ffi::c_void),
}

impl GenericHandle {
    pub fn is_null(&self) -> bool {
        match &self {
            GenericHandle::Ptr(p) => p.is_null(),
            GenericHandle::PtrMut(p) => p.is_null(),
        }
    }
}

impl From<*const core::ffi::c_void> for GenericHandle {
    fn from(ptr: *const core::ffi::c_void) -> Self {
        GenericHandle::Ptr(ptr)
    }
}

impl From<*mut core::ffi::c_void> for GenericHandle {
    fn from(ptr: *mut core::ffi::c_void) -> Self {
        GenericHandle::PtrMut(ptr)
    }
}

unsafe impl Send for GenericHandle {}

unsafe impl Sync for GenericHandle {}

mod detail {
    use std::sync::RwLockWriteGuard;

    use super::*;

    pub(crate) fn lock<'arc, 'app>(
        app: &'arc Arc<RwLock<Box<Application<'app>>>>,
    ) -> NuggetResult<RwLockWriteGuard<'arc, Box<Application<'app>>>> {
        Ok(app.write()?)
    }

    pub(crate) fn send_event(window_id: u32, event: impl Event, clients: &mut Vec<EventClient>) {
        for client in clients {
            let client_should_be_called = {
                client.client_id == window_id as i32 || {
                    // Check that the client id is a valid window id and if not call its callback
                    let window = unsafe { SDL_GetWindowFromID(client.client_id as u32) };
                    window.is_null()
                }
            };

            if client_should_be_called {
                if client.call(&event) == CallbackResult::Break {
                    break;
                }
            }
        }
    }
}

#[test]
fn create() -> NuggetResult<()> {
    if let Err(e) = Application::new() {
        return Err(e);
    }

    Ok(())
}

#[test]
fn instance() -> NuggetResult<()> {
    let _app = Application::new()?;
    assert!(Application::instance().is_some());

    Ok(())
}

#[test]
fn no_multiple_instances() -> NuggetResult<()> {
    let _app1 = Application::new()?;
    let app2 = Application::new();
    assert!(app2.is_err());
    assert!(unsafe { app2.unwrap_err_unchecked() } == ApplicationError::AlreadyExists.into());

    Ok(())
}

#[test]
fn post_callback() -> NuggetResult<()> {
    use std::sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    };

    let app = Application::new()?;

    let called: Arc<AtomicBool> = Arc::new(AtomicBool::new(false));
    let called_capture = Arc::clone(&called);
    app.write().unwrap().post_callback(move || {
        called_capture.store(true, Ordering::Release);
        if let Some(app) = Application::instance() {
            if let Ok(mut app) = app.write() {
                app.quit();
            } else {
                panic!();
            }
        } else {
            panic!();
        }
    })?;
    Application::exec(app)?;

    assert!(called.load(Ordering::Acquire));

    Ok(())
}
