use derivative::Derivative;

use crate::events::{Event, EventCategory, EventType, KeyCode};

/// Mouse Events
///
/// Events that are triggered as a result of mouse or touchscreen interaction.
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(u8)]
pub enum MouseButton {
    /// The primary (most often left) mouse button.
    Primary,
    /// The middle mouse button.
    Middle,
    /// The secondary (most often right) mouse button.
    Secondary,
    /// Extra button present on some mice.
    Button4,
    /// Extra button present on some mice.
    Button5,
    /// Extra button present on some mice.
    Button6,
    /// Extra button present on some mice.
    Button7,
    /// Extra button present on some mice.
    Button8,
    /// Extra button present on some mice.
    Button9,
    Count,
}

pub const MOUSE_BUTTON_COUNT: usize = MouseButton::Count as usize;

/// An event type that encapsulates mouse movement.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct MouseMoveEvent<'a> {
    /// The `x` coordinate of the event, with 0 representing the left-most point within the window
    pub x: i32,
    /// The relative `x` coordinate of the event, should be used when mouse is captured by the window
    pub rel_x: i32,
    /// The `y` coordinate of the event, with 0 representing the top-most point within the window
    pub y: i32,
    /// The relative `y` coordinate of the event, should be used when mouse is captured by the window
    pub rel_y: i32,
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
}

impl<'a> MouseMoveEvent<'a> {
    /// Constructs a new MouseMoveEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        x: i32,
        y: i32,
        rel_x: i32,
        rel_y: i32,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
    ) -> Self {
        Self {
            x,
            y,
            rel_x,
            rel_y,
            key_states,
            mouse_button_states,
        }
    }
}

impl Event for MouseMoveEvent<'_> {
    fn event_type() -> EventType {
        EventType::MouseMoved
    }
    fn event_categories() -> EventCategory {
        EventCategory::INPUT | EventCategory::MOUSE
    }
    fn event_name() -> &'static str {
        "MouseMoved"
    }
    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }
    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }
}

/// An event type that encapsulates a mouse button press action.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct MouseButtonPressEvent<'a> {
    /// The `x` coordinate of the event, with 0 representing the left-most point within the window
    pub x: i32,
    /// The `y` coordinate of the event, with 0 representing the top-most point within the window
    pub y: i32,
    pub button: MouseButton,
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
}

impl<'a> MouseButtonPressEvent<'a> {
    /// Constructs a new MouseButtonPressEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        x: i32,
        y: i32,
        button: u8,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
    ) -> Self {
        Self {
            x,
            y,
            button: unsafe { core::mem::transmute(button - 1) },
            key_states,
            mouse_button_states,
        }
    }
}

impl Event for MouseButtonPressEvent<'_> {
    fn event_type() -> EventType {
        EventType::MouseButtonPressed
    }
    fn event_categories() -> EventCategory {
        EventCategory::INPUT | EventCategory::MOUSE | EventCategory::MOUSE_BUTTON
    }
    fn event_name() -> &'static str {
        "MouseButtonPressed"
    }
    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }
    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }
}

/// An event type that encapsulates a mouse button release action.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct MouseButtonReleaseEvent<'a> {
    /// The `x` coordinate of the event, with 0 representing the left-most point within the window
    pub x: i32,
    /// The `y` coordinate of the event, with 0 representing the top-most point within the window
    pub y: i32,
    pub button: MouseButton,
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
}

impl<'a> MouseButtonReleaseEvent<'a> {
    /// Constructs a new MouseButtonReleaseEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        x: i32,
        y: i32,
        button: u8,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
    ) -> Self {
        Self {
            x,
            y,
            button: unsafe { core::mem::transmute(button - 1) },
            key_states,
            mouse_button_states,
        }
    }
}

impl Event for MouseButtonReleaseEvent<'_> {
    fn event_type() -> EventType {
        EventType::MouseButtonReleased
    }
    fn event_categories() -> EventCategory {
        EventCategory::INPUT | EventCategory::MOUSE | EventCategory::MOUSE_BUTTON
    }
    fn event_name() -> &'static str {
        "MouseButtonReleased"
    }
    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }
    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }
}

/// An event type that encapsulates a mouse scroll action using a wheel.
#[derive(Derivative)]
#[derivative(Debug)]
pub struct MouseWheelEvent<'a> {
    /// The amount scrolled horizontally.
    ///
    /// The value is positive if scrolled to the right and negative otherwise.
    pub x: i32,
    /// The amount scrolled vertically.
    ///
    /// The value is positive if scrolled to the away from the user and negative otherwise.
    pub y: i32,
    #[derivative(Debug = "ignore")]
    key_states: &'a [u8],
    #[derivative(Debug = "ignore")]
    mouse_button_states: &'a [u8],
}

impl<'a> MouseWheelEvent<'a> {
    /// Constructs a new MouseButtonPressEvent.
    ///
    /// Useless in user code. Used internally by Nugget to create new events that are then
    /// passed to registered event handlers.
    pub fn new(
        x: i32,
        y: i32,
        flipped: bool,
        key_states: &'a [u8],
        mouse_button_states: &'a [u8],
    ) -> Self {
        Self {
            x: if flipped { -x } else { x },
            y: if flipped { -y } else { y },
            key_states,
            mouse_button_states,
        }
    }
}

impl Event for MouseWheelEvent<'_> {
    fn event_type() -> EventType {
        EventType::MouseWheelTurned
    }
    fn event_categories() -> EventCategory {
        EventCategory::INPUT | EventCategory::MOUSE
    }
    fn event_name() -> &'static str {
        "MouseButtonReleased"
    }
    fn is_key_pressed(&self, code: KeyCode) -> bool {
        self.key_states[code as usize] != 0
    }
    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        self.mouse_button_states[button as usize] != 0
    }
}
