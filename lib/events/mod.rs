use bitflags::bitflags;

pub use key_events::*;
pub use mouse_events::*;
pub use render_event::*;
pub use window_events::*;

mod key_events;
mod mouse_events;
mod render_event;
mod window_events;

bitflags! {
    /// The board category of an event.
    pub struct EventCategory : u32 {
        const RENDER        = 1u32 << 0;
        const WINDOW        = 1u32 << 1;
        const INPUT         = 1u32 << 2;
        const KEYBOARD      = 1u32 << 3;
        const MOUSE         = 1u32 << 4;
        const MOUSE_BUTTON  = 1u32 << 5;
        const RESERVED1     = 1u32 << 6;
        const RESERVED2     = 1u32 << 7;
        const USER          = 1u32 << 8;
    }
}

/// Event identifiers known by Nugget.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum EventType {
    Render = 0,
    WindowShown,
    WindowHidden,
    WindowClosed,
    WindowResized,
    WindowMouseEntered,
    WindowMouseLeft,
    WindowGainedFocus,
    WindowLostFocus,
    WindowMoved,
    KeyPressed,
    KeyReleased,
    MouseButtonPressed,
    MouseButtonReleased,
    MouseMoved,
    MouseWheelTurned,
    User,
    Count,
}

pub const EVENT_TYPE_COUNT: usize = EventType::Count as usize;

/// Common description for all events in Nugget.
pub trait Event {
    /// The source of the event.
    fn event_type() -> EventType;
    /// The board category of the event.
    fn event_categories() -> EventCategory;
    /// The name of the event in human readable form.
    fn event_name() -> &'static str;
    /// Test if a certain key is pressed, on the keyboard, when the event occurs.
    fn is_key_pressed(&self, code: KeyCode) -> bool;
    /// Test if a mouse button is held down when the event occurs.
    fn is_mouse_button_pressed(&self, button: MouseButton) -> bool;
}
