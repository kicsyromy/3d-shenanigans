use std::fmt::{Display, Formatter};
use std::sync::{PoisonError, RwLockReadGuard, RwLockWriteGuard};

use crate::{rendering::RenderingError, Application};

/// Describes an windowing related errors
#[derive(Debug)]
pub enum WindowError {
    /// Failure to create a window.
    FailedToCreate(String),
    /// No instance of [Application](crate::Application) is available.
    NoApplicationInstance,
    /// Failed to set up or update the window's graphics context.
    RenderingError(RenderingError),
    /// Errors related to capturing or release the mouse
    MouseCaptureError(String),
    /// An unknown system error
    SystemError(Box<dyn std::error::Error>),
}

impl From<PoisonError<RwLockWriteGuard<'_, Box<Application<'_>>>>> for WindowError {
    fn from(_: PoisonError<RwLockWriteGuard<'_, Box<Application<'_>>>>) -> Self {
        WindowError::SystemError("Application RwLock was poisoned, buggy app?".into())
    }
}

impl From<PoisonError<RwLockReadGuard<'_, Box<Application<'_>>>>> for WindowError {
    fn from(_: PoisonError<RwLockReadGuard<'_, Box<Application<'_>>>>) -> Self {
        WindowError::SystemError("Application RwLock was poisoned, buggy app?".into())
    }
}

impl From<RenderingError> for WindowError {
    fn from(e: RenderingError) -> Self {
        WindowError::RenderingError(e)
    }
}

impl PartialEq for WindowError {
    fn eq(&self, other: &Self) -> bool {
        use WindowError::*;

        match &self {
            FailedToCreate(_) => {
                if let FailedToCreate(_) = &other {
                    true
                } else {
                    false
                }
            }
            NoApplicationInstance => {
                if let NoApplicationInstance = &other {
                    true
                } else {
                    false
                }
            }
            RenderingError(_) => {
                if let RenderingError(_) = &other {
                    true
                } else {
                    false
                }
            }
            MouseCaptureError(_) => {
                if let MouseCaptureError(_) = &other {
                    true
                } else {
                    false
                }
            }
            SystemError(_) => {
                if let SystemError(_) = &other {
                    true
                } else {
                    false
                }
            }
        }
    }
}

impl Display for WindowError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use WindowError::*;

        match &self {
            FailedToCreate(message) => {
                f.write_fmt(format_args!("Failed to create window: {}", message))
            }
            NoApplicationInstance => f.write_str(
                "An Application instance was not created or was destroyed before window creation",
            ),
            RenderingError(e) => e.fmt(f),
            MouseCaptureError(message) => f.write_str(&message),
            SystemError(error) => Display::fmt(&error, f),
        }
    }
}
